require('./channels.js')
const $path = require('path')
const $fs = require('fs')
const CONSTANTS = require('./constants')
const Error = require('./error')
const argv = require('yargs').argv
const Processes = require('./processes')
const validProcesses = [
  'documents',
  'scripts',
  'server',
  'styles',
  'copy',
  'media',
  'spawn'
]
if(argv.configuration) {
  if(argv.processGroup) {
    var configurationPath = $path.relative(
     __dirname,
      String.prototype.concat(
        argv.configuration,
          'process-groups',
          '/',
          argv.processGroup
      )
    )
    try {
      var ConfigurationData = require(configurationPath)
      Configuration = ConfigurationData
        .reduce((_configuration, process) => {
          let processPath = configurationPath
            .split('process-groups')
            .filter((fragment) => fragment.length)
            .slice(0, -1)
            .join('')
            .concat('processes/')
            .concat(process)
          _configuration[process] = require(processPath)
          return _configuration
        }, {})
      Processes(Configuration)
    } catch(error) {
      console.log(error)
    }
  } else if(argv.process) {
    var configurationPath = $path.relative(
     __dirname,
      String.prototype.concat(
        argv.configuration,
          'processes',
          '/',
          argv.process
      )
    )
    try {
      var process = argv.process
      var Configuration = {
        [process]: require(configurationPath)
      }
      Processes(Configuration)
    } catch(error) {
      console.log(error)
    }
  } else {
    throw new Error('Process Or Process Group Not Specified')
  }
} else {
  throw new Error('Configuration Not Specified')
}

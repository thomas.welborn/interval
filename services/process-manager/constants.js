const CONFIGURATION_NOT_SPECIFIED = 'CONFIGURATION_NOT_SPECIFIED'
const PROCESS_DOES_NOT_EXIST = 'PROCESS_DOES_NOT_EXIST'
const PROCESS_GROUP_PROCESS_DOES_NOT_EXIST = 'PROCESS_GROUP_PROCESS_DOES_NOT_EXIST'
const PROCESS_OR_PROCESS_GROUP_NOT_SPECIFIED = 'PROCESS_OR_PROCESS_GROUP_NOT_SPECIFIED'
const Constants = {
  CONFIGURATION_NOT_SPECIFIED: {
    type: CONFIGURATION_NOT_SPECIFIED,
    title: 'Configuration Not Specified',
    description: 'The flag --configuration must be specified with a directory path.'
  },
  PROCESS_OR_PROCESS_GROUP_NOT_SPECIFIED: {
    type: PROCESS_OR_PROCESS_GROUP_NOT_SPECIFIED,
    title: 'Process Or Process Group Not Specified',
    description: 'The flag --process or --process-group must be specified with a valid name.'
  },
  PROCESS_DOES_NOT_EXIST: {
    type: PROCESS_DOES_NOT_EXIST,
    title: 'Process Does Not Exist',
    description: 'The specified Process does not exist.'
  },
  PROCESS_GROUP_PROCESS_DOES_NOT_EXIST: {
    type: PROCESS_GROUP_PROCESS_DOES_NOT_EXIST,
    title: 'Process Group Process Does Not Exist',
    description: 'The specified Process Group Process does not exist.'
  },
}
module.exports = Constants

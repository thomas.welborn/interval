const Channels = require('mvc-framework/distribute/browser/channels')

global.Channels = global.Channels || new Channels()

module.exports = Channels

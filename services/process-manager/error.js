const Error = (data) => {
  data.title = 'Title: '.concat(data.title)
  data.description = 'Description: '.concat(data.description)
  data.type = 'Type: '.concat(data.type)
  let separatorLength = [
    data.title,
    data.description,
    data.type,
  ]
  .reduce((_separatorLength, dataType) => {
    _separatorLength = (dataType.length > _separatorLength)
      ? dataType.length
      : _separatorLength
    return _separatorLength
  }, 0)
  let separatorCharacter = '—'
  let separatorString = Array
    .apply(0, Array(separatorLength))
    .fill(separatorCharacter, 0).join('')
  console.error(
    '\n',
    separatorString,
    '\n',
    data.title,
    '\n',
    data.description,
    '\n',
    data.type,
    '\n',
    separatorString,
  )
}
module.exports = Error

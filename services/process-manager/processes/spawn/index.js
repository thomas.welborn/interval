const Spawn = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'spawn') {
        let spawn = require('./spawn.js')
        spawn(configurationSettings)
      }
    })
}

module.exports = Spawn

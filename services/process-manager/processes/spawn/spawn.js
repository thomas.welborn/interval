const chokidar = require('chokidar')
const argv = require('yargs').argv
const $spawn = require('child_process').spawnSync
const $path = require('path')

const Spawn = async (configuration) => {
  let browserSync
  try {
    browserSync = Channels.channel('BrowserSync')
      .request('getBrowserSync')
      .emitter.emit('exit')
  } catch(error) {}
  const processName = 'node'
  const processParameters = process.argv.slice(1)
  let childProcess = $spawn(
    processName,
    processParameters,
    configuration.spawn.options
  )
  try {
    browserSync.reload()
  } catch(error) {}
  return
}

const Watch = (configuration) => {
  const watcher = chokidar.watch(
    configuration.watch.include,
    configuration.watch.options || {},
  )
  watcher.on('change', (path, stats) => {
    // console.log(path, '\n', stats)
    Spawn(configuration)
  })
}

module.exports = Watch

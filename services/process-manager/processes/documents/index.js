const Documents = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'ejs') {
        let ejs = require('./ejs.js')
        ejs(configurationSettings)
      }
    })
}

module.exports = Documents

const $fs = require('fs')
const $path = require('path')
const ejs = require('ejs')
const globby = require('globby')
const chokidar = require('chokidar')
const beautify = require('js-beautify')
const Destination = require('../utilities/destination.js')
const Watch = require('../utilities/watch.js')

const renderDocuments = async (configuration) => {
  const destinationPathNames = configuration.destination.pathnames
  const destinationOptions = configuration.destination.options
  const destinationFileName = configuration.rename.filename
    .concat(configuration.rename.extension)
  const ejsTemplatePath = $path.resolve(configuration.ejs.template)
  const ejsTemplate = await $fs.readFileSync(ejsTemplatePath, 'utf8')
  const ejsOptions = configuration.ejs.options
  const paths = await globby(configuration.source.globs)
  paths
    .forEach(async (glob) => {
      let templateDataPath = $path.resolve(glob)
      let templateData = require(templateDataPath)
      let template = await ejs.renderFile(ejsTemplatePath, templateData, ejsOptions)
      if(configuration.beautify) {
        let beautifyOptions = configuration.beautify
        template = beautify.html(template, beautifyOptions)
      }
      new Destination(
        template,
        destinationPathNames,
        destinationFileName,
        {
          encoding: 'utf8'
        }
      )
    })
  return
}
const Documents = (configuration) => {
  if(configuration.watch) {
    new Watch(
      configuration.watch.paths,
      configuration.watch.options,
      renderDocuments,
      configuration
    )
  } else {
    renderDocuments(configuration)
  }
  return
}
module.exports =  Documents

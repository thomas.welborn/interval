const Server = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'browserSync') {
        let browserSync = require('./browser-sync.js')
        browserSync(configurationSettings)
      }
    })
}

module.exports = Server

const BrowserSync = require('browser-sync')

const Server = async (configuration) => {
  let browserSyncName = configuration.browserSync.create
  let browserSyncInitOptions = configuration.browserSync.init
  let browserSync
  console.log('BrowserSync.has(browserSyncName)', BrowserSync.has(browserSyncName))
  if(BrowserSync.has(browserSyncName)) {
    delete browserSyncInitOptions.browser
    browserSyncInitOptions.open = false
    browserSync = BrowserSync.get(browserSyncName)
    browserSync.exit()
  } else {
    browserSync = BrowserSync.create(browserSyncName)
    browserSync.emitter.on('exit', async () => {
      return await browserSync.exit()
    })
  }
  browserSync.init(browserSyncInitOptions)
  Channels.channel('BrowserSync')
    .response(
      'getBrowserSync',
      (event) => {
        return browserSync
      }
    )
  return
}

module.exports = Server

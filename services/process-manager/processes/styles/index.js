const Styles = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'sass') {
        let sass = require('./sass.js')
        sass(configurationSettings)
      }
    })
}

module.exports = Styles

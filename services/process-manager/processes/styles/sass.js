const $fs = require('fs')
const $path = require('path')
const sass = require('node-sass')
const Watch = require('../utilities/watch.js')

const renderStyles = (configuration) => {
  let sassOptions = configuration.sass
  let destinationOptions = configuration.destination
  let destinationPathnames = destinationOptions.pathnames
  let bundle = sass.renderSync(sassOptions)
  let filename = String.prototype
    .concat(
      configuration.rename.filename,
      configuration.rename.extension
    )
  destinationPathnames
    .forEach((destinationPathname) => {
      let destination = $path.join(
        destinationPathname,
        filename
      )
      $fs.mkdirSync(destinationPathname, {
        recursive: true,
      })
      $fs.writeFileSync(destination, bundle.css)
    })
  return
}
const Styles = async (configuration) => {
  if(configuration.watch) {
    new Watch(
      configuration.watch.paths,
      configuration.watch.options,
      renderStyles,
      configuration
    )
  } else {
    renderStyles(configuration)
  }
}

module.exports = Styles

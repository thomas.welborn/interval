const Scripts = (configuration) => {
  const promises = Object.entries(configuration.processes)
  .reduce((_promises, [configurationName, configurationSettings]) => {
    let promise
    switch(configurationSettings.type) {
      case 'spritesmith':
      let spritesmith = require('./spritesmith.js')
      promise = new Promise((resolve, reject) => {
        spritesmith(configurationSettings)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
      })
      _promises.push(promise)
      break

      case 'gm':
      let gm = require('./gm.js')
      promise = new Promise((resolve, reject) => {
        gm(configurationSettings)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
      })
      _promises.push(promise)
      break
    }
    return _promises
  }, [])
  return Promise.all(promises)
  .then((response) => {
    console.log('Success')
    return
  })
  .catch((error) => {
    console.log('Error', error)
    return
  })
}

module.exports = Scripts

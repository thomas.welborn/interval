const $fs = require('fs')
const $path = require('path')
const del = require('del')
const globby = require('globby')
const Watch = require('../utilities/watch.js')
const Paths = require('../../subprocesses/paths')
const DelSync = require('../../subprocesses/delsync')
const MkDirSync = require('../../subprocesses/mkdirsync')
const Spritesmith = require('spritesmith')

const renderMedia = (configuration) => {
  const validParameters = [
    'engine',
    'engineOpts',
    'src',
    'padding',
    'algorithm',
    'algorithmOpts'
  ]
  const validEngines = [
    'pixelsmith',
    'phantomjspixelsmith',
    'canvassmith',
    'gmsmith',
    'imagemagick'
  ]
  const destinationOptions = configuration.destination
  const destinationPathnames = destinationOptions.pathnames
  console.log('Clearing Destination Paths')
  destinationPathnames.forEach((destinationPathname) => {
    console.log('Clear', destinationPathname)
    DelSync($path.join(destinationPathname, '**'))
    MkDirSync(destinationPathname)
  })
  const parameters = validParameters.reduce((_parameters, validParameter) => {
    switch(validParameter) {
      case 'engine':
        validEngines.forEach((validEngine) => {
          if(
            configuration.spritesmith[validParameter] &&
            configuration.spritesmith[validParameter][validEngine] === true
          ) {
            _parameters[validParameter] = require(validEngine)
          }
        })
        break
      case 'src':
        if(configuration.spritesmith[validParameter]) {
          _parameters[validParameter] = Paths(
            configuration.spritesmith[validParameter].globs,
            configuration.spritesmith[validParameter].options
          )
        }
        break
      default:
        if(configuration.spritesmith[validParameter]) {
          _parameters[validParameter] = configuration.spritesmith[validParameter]
        }
        break
    }
    return _parameters
  }, {})
  let filename = String.prototype
    .concat(
      configuration.rename.filename,
      configuration.rename.extension
    )
  let errors = []
  let promise = new Promise((resolve, reject) => {
    console.log('Start Spritesmith')
    console.log('Source', parameters.src)
    Spritesmith.run(parameters, (error, result) => {
      if(error) errors.push(new Error('reject'))
      console.log('Result')
      console.log('Coordinates', result.coordinates)
      console.log('Properties', result.properties)
      destinationPathnames
      .forEach((destinationPathname, destinationPathnameIndex) => {
        let destination = $path.join(
          destinationPathname,
          filename
        )
        console.log('Destination', destination)
        $fs.writeFileSync(destination, result.image, configuration.destination.options || {})
        if(destinationPathnameIndex === destinationPathnames.length - 1) {
          if(errors.length === 0) {
            resolve(true)
          } else {
            reject(errors)
          }
          console.log('Stop Spritesmith')
        }
      })
    })
  })
  return promise
}
const Media = (configuration) => {
  if(configuration.watch) {
    new Watch(
      configuration.watch.paths,
      configuration.watch.options,
      renderMedia,
      configuration
    )
  }
  return renderMedia(configuration)
}

module.exports = Media

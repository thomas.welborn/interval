const $fs = require('fs')
const $path = require('path')
const gm = require('gm')
const Watch = require('../utilities/watch.js')
const Paths = require('../../subprocesses/paths')
const DelSync = require('../../subprocesses/delsync')
const MkDirSync = require('../../subprocesses/mkdirsync')

const renderMedia = (configuration) => {
  const validParameters = [
    'resize',
    'quality'
  ]
  const destinationPathnames = configuration.destination.pathnames
  const destinationOptions = configuration.destination.options
  const sourcePaths = Paths(configuration.source.globs, configuration.source.options)
  console.log('Clearing Destination Paths')
  destinationPathnames.forEach((destinationPathname) => {
    console.log('Clear', destinationPathname)
    DelSync($path.join(destinationPathname, '**'))
    MkDirSync(destinationPathname)
  })
  const promises = sourcePaths.reduce((_promises, path, pathIndex) => {
    let promise = new Promise((resolve, reject) => {
      let errors = []
      let chain = gm(path)
      validParameters.forEach((validParameter) => {
        if(configuration.gm[validParameter]) {
          switch(validParameter) {
            case 'resize':
              chain[validParameter](...configuration.gm[validParameter])
              break
            case 'flatten':
              if(configuration.gm[validParameter] === true) chain[validParameter]()
              break
            default:
              chain[validParameter](configuration.gm[validParameter])
              break
          }
        }
      })
      chain.toBuffer(null, (error, buffer) => {
        if(error) errors.push(new Error('reject'))
        destinationPathnames.forEach((destinationPathname, destinationPathnameIndex) => {
          let destinationFileName = $path.basename(path)
          let destinationFileExtension = $path.extname(path)
          let destination = $path.join(
            destinationPathname,
            destinationFileName
          )
          console.log('Start GraphicsMagick')
          console.log('Source', path)
          console.log('Destination', destination)
          $fs.writeFileSync(destination, buffer, configuration.destination.options || {})
          if(destinationPathnameIndex === destinationPathnames.length - 1) {
            if(errors.length === 0) {
              resolve(true)
            } else {
              reject(errors)
            }
            console.log('Stop GraphicsMagick')
          }
        })
      })
    })
    _promises.push(promise)
    return _promises
  }, [])
  return Promise.all(promises)
}
const Media = (configuration) => {
  if(configuration.watch) {
    new Watch(
      configuration.watch.paths,
      configuration.watch.options,
      renderMedia,
      configuration
    )
  }
  return renderMedia(configuration)
}

module.exports = Media

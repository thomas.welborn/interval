const chokidar = require('chokidar')
class Watch {
  constructor(watchPaths, watchOptions, callback) {
    let callbackArguments = Array.from(arguments).slice(3)
    let watch = chokidar.watch(watchPaths, watchOptions)
    watch
      .on('add', path => callback(...callbackArguments))
      .on('change', path => callback(...callbackArguments))
  }
}
module.exports = Watch

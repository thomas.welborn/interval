const $fs = require('fs')
const $path = require('path')

class Destination {
  constructor(file, pathNames, fileName, options) {
    const _arguments = Array.from(arguments)
    this.validArguments
      .forEach((propertyKey, propertyIndex) => {
        this[propertyKey] = _arguments[propertyIndex]
      })
    this.ready = (
      this.pathNames
        .reduce(
          (_ready, pathName) => {
            this
              .writeFile(pathName)
            _ready.push(true)
            return _ready
          },
          []
        ).indexOf(-1) === false
    ) ? true
      : false
  }
  get validArguments() { return [
    'file',
    'pathNames',
    'fileName',
    'options',
  ] }
  get ready() {
    return this._ready
  }
  set ready(ready) {
    this._ready = ready
  }
  get file() {
    return this._file
  }
  set file(file) {
    this._file = file
  }
  get pathNames() {
    return this._pathNames
  }
  set pathNames(pathNames) {
    this._pathNames = pathNames
  }
  get fileName() {
    return this._fileName
  }
  set fileName(fileName) {
    this._fileName = fileName
  }
  get options() {
    return this._options
  }
  set options(options) {
    this._options = options
  }
  getDestination(pathName) {
    return $path.join(
      pathName,
      this.fileName
    )
  }
  writeFile(pathName) {
    $fs.mkdirSync(
      $path.join(
        pathName,
        $path.dirname(this.fileName)
      ),
      {
        recursive: true,
      }
    )
    $fs.writeFileSync(
      this.getDestination(pathName),
      this.file,
    )
    return this
  }
}

module.exports = Destination

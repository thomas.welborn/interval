const Copy = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'copyFile') {
        let copyFile = require('./copy-file.js')
        copyFile(configurationSettings)
      }
    })
}

module.exports = Copy

const $fs = require('fs')
const $path = require('path')
const globby = require('globby')
const mergeFiles = require('merge-files')
const uglify = require('uglify-js')
const chokidar = require('chokidar')
const Destination = require('../utilities/destination.js')

const CopyFiles = async (configuration) => {
  let sourceEncoding = configuration.source.options.encoding
  let destinationPathNames = configuration.destination.pathnames
  let destinationOptions = configuration.destination.options
  let destinationEncoding = configuration.destination.options.encoding
  let paths = await globby(configuration.source.globs)
  if(configuration.concatenate) {
    var destinationFileName = configuration.rename.filename
      .concat(configuration.rename.extension)
    destinationPathNames
      .forEach(async (destinationPathName) => {
        var destination = $path.join(destinationPathName, destinationFileName)
        var merge = await mergeFiles(paths, destination)
      })
  } else {
    paths
      .forEach(async (path) => {
        let pathData = $path.parse(path)
        var destinationFileName
        if(configuration.rename) {
          destinationFileName = configuration.rename.filenames[path]
        } else {
          destinationFileName = pathData.base
        }
        let file = await $fs.readFileSync($path.resolve(path), sourceEncoding)
        new Destination(
          file,
          destinationPathNames,
          destinationFileName,
          destinationOptions
        )
      })
  }
}

const Copy = (configuration) => {
  if(configuration.watch) {
    const watcher = chokidar.watch(
      configuration.watch.include,
      configuration.watch.options || {},
    )
    watcher.on('change', (path, stats) => {
      console.log(stats)
      CopyFiles(configuration)
      try {
        let browserSync = Channels.channel('BrowserSync')
          .request('getBrowserSync')
        browserSync.reload()
      } catch(error) {}
    })
  } else {
    CopyFiles(configuration)
  }
}

module.exports = Copy

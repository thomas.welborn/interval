const Scripts = (configuration) => {
  Object.entries(configuration.processes)
    .forEach(([configurationName, configurationSettings]) => {
      if(configurationSettings.type === 'rollup') {
        let rollup = require('./rollup.js')
        rollup(configurationSettings)
      }
    })
}

module.exports = Scripts

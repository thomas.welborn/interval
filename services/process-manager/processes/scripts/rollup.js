const path = require('path')
const rollup = require('rollup')

const Scripts = async (configuration) => {
  let inputOptions = configuration.rollup.input
  let externalOptions = configuration.rollup.external
  let pluginOptions = configuration.rollup.plugins
    .reduce((_plugins, [pluginName, pluginSettings]) => {
      let plugin = require(pluginName)
      _plugins.push(plugin(pluginSettings))
      return _plugins
    }, [])
  let outputOptions = configuration.rollup.output
  let watchOptions = configuration.rollup.watch
  if(watchOptions) {
    let rollupWatch = rollup.watch({
      input: inputOptions,
      plugins: pluginOptions,
      external: externalOptions,
      output: outputOptions,
      watch: watchOptions,
    })
    rollupWatch
      .on('event', event => {
        switch(event.code) {
          case 'START':
            console.log('START', event)
            break
          case 'BUNDLE_START':
            console.log('BUNDLE_START', event)
            break
          case 'BUNDLE_END':
            console.log('BUNDLE_END', event)
            break
          case 'END':
            console.log('END', event)
            break
          case 'ERROR':
            console.log('ERROR', event)
            break
          case 'FATAL':
            console.log('FATAL')
            break
        }
      })
  } else {
    let bundle = await rollup.rollup({
      input: inputOptions,
      plugins: pluginOptions,
      external: externalOptions,
    })
    await bundle.write(outputOptions)
  }
}

module.exports = Scripts

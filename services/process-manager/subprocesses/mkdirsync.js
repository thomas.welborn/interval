const $fs = require('fs')
const del = require('del')

const MkDirSync = (pathname) => {
  $fs.mkdirSync(pathname, {
    recursive: true,
  })
  return
}

module.exports = MkDirSync

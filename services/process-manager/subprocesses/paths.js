const globby = require('globby')

const Paths = (paths = [], options = {}) => {
  return globby.sync(paths, options)
}
module.exports = Paths

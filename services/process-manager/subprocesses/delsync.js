const del = require('del')
const $path = require('path')
const DelSync = (pathname) => {
  del.sync($path.join(pathname))
  return
}
module.exports = DelSync

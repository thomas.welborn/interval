const ValidSettingsErrorTemplate = validSettings => `
⚠ Invalid Setting${
  validSettings.invalid.required.length > 1
    ? 's'
    : ''
}:
${
  validSettings.invalid.map((invalidSetting) => {
    return `   - ${invalidSetting.name}`
  }).join('\n')
}
`

export default class Interval {
  constructor(settings) {
    if(settings) this.settings = settings
  }
  get validSettings() { return [
    {
      name: 'intervalDuration',
      required: true,
      validate: value => (
        typeof value === 'number' &&
        value >= 0
      ),
    },
    {
      name: 'callback',
      required: true,
      validate: value => typeof value === 'function',
    },
    {
      name: 'beforeSet',
      required: true,
      validate: value => typeof value === 'boolean',
    },
    {
      name: 'afterClear',
      required: true,
      validate: value => typeof value === 'boolean',
    },
    {
      name: 'holdDuration',
      required: true,
      validate: value => (
        typeof value === 'number' &&
        value >= 0
      ),
    },
    {
      name: 'maximumElapsed',
      required: false,
      validate: value => (
        typeof value === 'number' &&
        value >= 0
      ),
    },
  ] }
  get settings() { return this._settings }
  set settings(settings) {
    const validSettings = this.validSettings
      .reduce((_validSettings, validSetting, validSettingIndex) => {
        const setting = settings[validSetting.name]
        const valid = validSetting.validate(setting)
        if(
          !valid
        ) {
          if(validSetting.required) {
            _validSettings.invalid.required.push(validSetting)
          } else if(!validSetting.required) {
            _validSettings.invalid.unrequired.push(validSetting)
          }
        }
        if(validSettingIndex === this.validSettings.length - 1) {
          if(_validSettings.invalid.required.length === 0) {
            _validSettings.isValid = true
          } else {
            _validSettings.isValid = false
          }
        }
        return _validSettings
      }, {
        invalid: {
          required: [],
          unrequired: [],
        },
        isValid: null,
      })
    console.log(validSettings)
    if(validSettings.isValid === false) {
      throw new Error(ValidSettingsErrorTemplate(validSettings))
    }
    // this.validSettings
    //   .forEach((validSetting) => {
    //     if(
    //       typeof settings[validSetting] !== 'undefined'
    //     ) this[validSetting] = settings[validSetting]
    //   })
    // this._settings = settings
  }
  get intervalCallback() {
    return () => {
      if(!this.isMaximumElapsed) {
        this.intervals += 1
        this.elapsed = this.intervals * this.intervalDuration
        this.callback(this.parse())
      } else if(this.isMaximumElapsed) {
        this.stop()
      }
    }
  }
  get timeoutCallback() {
    return () => {
      this.isHoldElapsed = true
      this.isHolding = false
      this.start()
    }
  }
  get interval() { return this._interval || null }
  set interval(interval) { this._interval = interval }
  get timeout() { return this._timeout || null }
  set timeout(timeout) { this._timeout = timeout }
  get intervals() { return this._intervals || 0 }
  set intervals(intervals) { this._intervals = intervals }
  get callback() {
    if(typeof this._callback !== 'function') {
      this._callback = () => {}
    }
    return this._callback
  }
  set callback(callback) { this._callback = callback }
  get intervalDuration() { return this._intervalDuration || 0 }
  set intervalDuration(intervalDuration) { this._intervalDuration = intervalDuration }
  get elapsed() { return this._elapsed || 0 }
  set elapsed(elapsed) { this._elapsed = elapsed }
  get beforeSet() { return (typeof this._beforeSet === 'boolean')
    ? this._beforeSet
    : false }
  set beforeSet(beforeSet) { this._beforeSet = beforeSet }
  get afterClear() { return (typeof this._afterClear === 'boolean')
    ? this._afterClear
    : false }
  set afterClear(afterClear) { this._afterClear = afterClear }
  get holdDuration() {
    return (typeof this._holdDuration === 'number')
      ? this._holdDuration
      : 0
  }
  set holdDuration(holdDuration) { this._holdDuration = holdDuration }
  get maximumElapsed() {
    return (typeof this._maximumElapsed === 'number')
      ? this._maximumElapsed
      : Infinity
  }
  set maximumElapsed(maximumElapsed) { this._maximumElapsed = maximumElapsed }
  get isActive() {
    return (typeof this._isActive === 'boolean')
      ? this._isActive
      : false
  }
  set isActive(isActive) { this._isActive = isActive }
  get isHoldElapsed() {
    return (typeof this._isHoldElapsed === 'boolean')
      ? this._isHoldElapsed
      : false
  }
  set isHoldElapsed(isHoldElapsed) { this._isHoldElapsed = isHoldElapsed }
  get isHolding() {
    return (typeof this._isHolding === 'number')
      ? this._isHolding
      : false
  }
  set isHolding(isHolding) { this._isHolding = isHolding }
  get isMaximumElapsed() {
    return (this.elapsed >= this.maximumElapsed)
      ? true
      : false
  }
  parse() {
    return {
      isActive: this.isActive,
      isMaximumElapsed: this.isMaximumElapsed,
      intervals: this.intervals,
      elapsed: this.elapsed,
      intervalDuration: this.intervalDuration,
      holdDuration: this.holdDuration,
      maximumElapsed: this.maximumElapsed,
    }
  }
  clearTimeout() {
    if(this.timeout) {
      this.timeout = clearTimeout(this.timeout)
    }
    return this
  }
  clearInterval() {
    if(
      this.afterClear === true
    ) {
      this.callback({
        ...this.parse(),
        isAfterClear: true,
        intervalDuration: 0,
      })
    }
    if(this.interval) {
      this.interval = clearInterval(this.interval)
      this.intervals = 0
      this.elapsed = 0
    }
    return this
  }
  start() {
    if(!this.settings) throw new Error('⚠ Settings Must Be Defined.')
    console.log('keeps going')
    if(
      this.isHoldElapsed &&
      !this.isActive &&
      !this.isMaximumElapsed &&
      !this.isHolding
    ) {
      if(this.beforeSet === true) {
        this.callback({
          ...this.parse(),
          isBeforeSet: true,
          intervalDuration: 0,
        })
      }
      this.isActive = true
      this.interval = setInterval(
        this.intervalCallback,
        this.intervalDuration,
      )
    } else if(
      !this.isHoldElapsed &&
      !this.timeout
    ) {
      this.isHolding = true
      this.timeout = setTimeout(
        this.timeoutCallback,
        this.holdDuration
      )
    }
    return this
  }
  stop() {
    if(this.isActive) {
      this.isActive = false
      this.clearInterval()
    } else if(!this.isActive) {
      throw new Error(`
        ⚠ Interval Is Not Active.
        ${
          (!this.settings)
          ? "⚠ Settings Must Be Defined."
          : null
        }
      `)
    }
    if(this.timeout) {
      this.isHoldElapsed = false
      this.isHolding = false
      this.clearTimeout()
    }
  }
  initialize(settings) {
    this.settings = settings
    return this
  }
}
